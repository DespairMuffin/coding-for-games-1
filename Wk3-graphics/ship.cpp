#include "ship.h"

Ship::Ship() : Entity()
{
	spriteData.width  = shipNS::WIDTH;
	spriteData.height = shipNS::HEIGHT;
	spriteData.x	  = shipNS::X;
	spriteData.y      = shipNS::Y;
	spriteData.rect.bottom = shipNS::HEIGHT;
	spriteData.rect.right  = shipNS::WIDTH;

	frameDelay   = shipNS::SHIP_ANIMATION_DELAY;
	startFrame   = shipNS::SHIP1_START_FRAME;
	endFrame     = shipNS::SHIP1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius        = shipNS::WIDTH/2.0f;

	velocity.x = 0;
	velocity.y = 0;
}

void Ship::update ( float frameTime )
{
	Entity::update( frameTime );
	
	// Rotation & position
	spriteData.angle += frameTime * shipNS::ROTATION_RATE;
	spriteData.x     += frameTime * velocity.x;
	spriteData.y     += frameTime * velocity.y;
	
	// Bounce of the walls of the screen!
	if( spriteData.x > ( GAME_WIDTH - shipNS::WIDTH * getScale() ) )
	{
		// Push it back onto the screen if it's gone past the edge
		spriteData.x = GAME_WIDTH - shipNS::WIDTH * getScale();
		velocity.x = -velocity.x;
	}
	else if ( spriteData.x < 0 )
	{
		spriteData.x = 0;
		velocity.x = -velocity.x;
	}

	if( spriteData.y > ( GAME_HEIGHT - shipNS::HEIGHT * getScale() ) )
	{
		// Push it back onto the screen if it's gone past the edge
		spriteData.y = GAME_HEIGHT - shipNS::HEIGHT * getScale();
		velocity.y = -velocity.y;
	}
	else if ( spriteData.y < 0 )
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;
	}

	if( shieldOn )
	{
		shield.update( frameTime );
		if( shield.getAnimationComplete() )
		{
			shieldOn = false;
			shield.setAnimationComplete( false );		// Reset animation
			shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
		}
	}
}

bool Ship::initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* texturePtr )
{
	shield.initialize( gamePtr->getGraphics(), width, height, ncols, texturePtr );
	shield.setFrames( shipNS::SHIELD_START_FRAME, shipNS::SHIELD_END_FRAME );
	shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
	shield.setFrameDelay( shipNS::SHIELD_ANIMATION_DELAY );
	shield.setLoop( false );		// Don't want to loop the animation
	return( Entity::initialize( gamePtr, width, height, ncols, texturePtr ) );
}

void Ship::draw()
{
	Image::draw();		// Draw the ship first

	if( shieldOn )		// If the shield is on, draw it on top of the ship
	{
		shield.draw( spriteData, graphicsNS::ALPHA50 & colorFilter );
		// Ship's spritedata -- shield will have time 
	}
}

void Ship::damage( WEAPON weapon )
{
	shieldOn = true;
}
#include "spacewar.h"

SpaceWar::SpaceWar()
{}

SpaceWar::~SpaceWar()
{
	releaseAll();
}

//initialize the game
void SpaceWar::initialize( HWND hwnd )
{
	Game::initialize( hwnd );  //call game's initialize

	if( !nebulaTexture.initialize( graphics, NEBULA_IMAGE ) )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, " Error initializing nebula texture" ) );
	}

	if(  !gameTextures.initialize( graphics, TEXTURES_IMAGE ) )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, " Error initializing game textures" ) );
	}

	if( !nebula.initialize( graphics, 0, 0, 0, &nebulaTexture ) )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, " Error initializing nebula texture" ) );
	}

	if( !planet.initialize( this, planetNS::WIDTH, planetNS::HEIGHT, planetNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, " Error initializing planet texture" ) );
	}

	// Place the planet in the center of the screen
	planet.setX( GAME_WIDTH*0.5f  - planet.getWidth()*0.5f );
	planet.setY( GAME_HEIGHT*0.5f - planet.getHeight()*0.5f );

	if( !ship1.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, " Error initializing ship texture" ) );
	}

	// Starting screen position for ship 1
	ship1.setX( GAME_WIDTH/4 );
	ship1.setY( GAME_HEIGHT/4 );

	ship1.setFrames( shipNS::SHIP1_START_FRAME, shipNS::SHIP1_END_FRAME );		// Set up the animtation frames
	ship1.setCurrentFrame( shipNS::SHIP1_START_FRAME );				// Starting frame
	ship1.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );				// How long to show each frame 

	ship1.setVelocity( VECTOR2( shipNS::SPEED, -shipNS::SPEED ) );

	if( !ship2.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, " Error initializing ship texture" ) );
	}

	// Starting screen position for ship 2
	ship2.setX( GAME_WIDTH/4 );
	ship2.setY( GAME_HEIGHT/4 );

	ship2.setFrames( shipNS::SHIP2_START_FRAME, shipNS::SHIP2_END_FRAME );		// Set up the animtation frames
	ship2.setCurrentFrame( shipNS::SHIP2_START_FRAME );				// Starting frame
	ship2.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );				// How long to show each frame 

	ship2.setVelocity( VECTOR2( shipNS::SPEED, -shipNS::SPEED ) );

	return;
}

void SpaceWar::update()
{
	// Makes the ship attracted to the planet
	ship1.gravityForce( &planet, frameTime );

	// Animate our ship
	ship1.update( frameTime );
	ship2.update( frameTime );
	planet.update( frameTime );

	/*
	// Rotate
	ship.setDegrees( ship.getDegrees() + frameTime * 200.0f );

	// Scale 
	ship.setScale( ship.getScale() - frameTime * 0.2f );

	ship.setX( ship.getX() + frameTime * 100.0f );

	if( ship.getX() > GAME_WIDTH )	// If offscreen right
	{
		ship.setX( ( float ) - ship.getWidth() );	// Position offscreen left
		ship.setScale( 1.5f );						// Reset the scale to 1.5
	}
	*/

	/*
	if( input->isKeyDown( SHIP_RIGHT_KEY ) || input->isKeyDown( D_KEY ) )
	{
		if( ship.getX() < ( GAME_WIDTH - ship.getWidth() ) )
		{
			ship.setX( ship.getX() + frameTime * SHIP_SPEED );
		}
	}
	
	if( input->isKeyDown( SHIP_LEFT_KEY ) || input->isKeyDown( A_KEY ) )
	{
		if( ship.getX() > 0 )
		{
			ship.setX( ship.getX() - frameTime * SHIP_SPEED );
		}
	}
	
	if( input->isKeyDown( SHIP_UP_KEY ) || input->isKeyDown( W_KEY ) )
	{
		if( ship.getY() > 0 )
		{
			ship.setY( ship.getY() - frameTime * SHIP_SPEED );
		}
	}
	
	if( input->isKeyDown( SHIP_DOWN_KEY ) || input->isKeyDown( S_KEY ))
	{
		if( ship.getY() < ( GAME_HEIGHT - ship.getHeight() ) )
		{
			ship.setY( ship.getY() + frameTime * SHIP_SPEED );
		}
	}
	*/
}

void SpaceWar::ai()
{

}

void SpaceWar::collisions()
{
	VECTOR2 collisionVector;
	if( ship1.collidesWith( planet, collisionVector ) )
	{
		// bounce off the planet
		ship1.bounce( collisionVector, planet );
		ship1.damage( PLANET );
	}

	if( ship2.collidesWith( planet, collisionVector ) )
	{
		// bounce off the planet
		ship2.bounce( collisionVector, planet );
		ship2.damage( PLANET );
	}

	if( ship1.collidesWith( ship2, collisionVector ) )
	{
		
		ship1.bounce( collisionVector, ship2 );
		ship1.damage( SHIP );

		ship2.bounce( collisionVector * -1, ship1 );		// 
		ship2.damage( SHIP );
	}
}

void SpaceWar::render()
{
	graphics -> spriteBegin();
	// Call draw function after!

	nebula.draw();
	planet.draw();
	ship1.draw();
	ship2.draw();

	// call draw function before!
	graphics -> spriteEnd();
}

void SpaceWar::releaseAll()
{
	gameTextures.onLostDevice();
	nebulaTexture.onLostDevice();
	Game::releaseAll();

}

void SpaceWar::resetAll()
{
	gameTextures.onResetDevice();
	nebulaTexture.onResetDevice();
	Game::resetAll();
}
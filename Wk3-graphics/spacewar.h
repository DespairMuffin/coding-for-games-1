#ifndef _SPACEWAR_H
#define _SPACEWAR_H
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "ship.h"
#include "planet.h"

class SpaceWar : public Game
{
private:
	//variables
	TextureManager nebulaTexture;	// Nebula Texture
	//TextureManager planetTexture;	// Planet Texture
	//TextureManager shipTexture;		// Ship Texture
	TextureManager gameTextures;

	Image nebula;	// Nebula image
	Planet planet;	// Planet image
	Ship ship1;		// Ship 1
	Ship ship2;     // Ship 2

public:
	SpaceWar();  // constructor
	virtual ~SpaceWar();  // destructor

	void initialize( HWND hwnd );

	void update();
	void ai();
	void collisions();
	void render();

	void releaseAll();
	void resetAll();
};

#endif